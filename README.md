# hsf-pandora-boot-sample

- 时间：2019-11-22

## 1. HSF缺点

HSF的缺点是其要使用指定的JBoss等容器，还需要在JBoss等容器中加入sar包扩展，对用户运行环境的侵入性大，如果你要运行在Weblogic或Websphere等其它容器上，需要自行扩展容器以兼容HSF的ClassLoader加载。 taobao有类似的其他框架Dubbo,介绍见
http://www.iteye.com/magazines/103

## 2. Dubbo和淘宝HSF相比

1. **Dubbo比HSF的部署方式更轻量**，HSF要求使用指定的JBoss等容器，还需要在JBoss等容器中加入sar包扩展，对用户运行环境的侵入性大，如果你要运行在Weblogic或Websphere等其它容器上，需要自行扩展容器以兼容HSF的ClassLoader加载，而Dubbo没有任何要求，可运行在任何Java环境中。
2. **Dubbo比HSF的扩展性更好，很方便二次开发**，一个框架不可能覆盖所有需求，Dubbo始终保持平等对待第三方理念，即所有功能，都可以在不修改Dubbo原生代码的情况下，在外围扩展，包括Dubbo自己内置的功能，也和第三方一样，是通过扩展的方式实现的，而HSF如果你要加功能或替换某部分实现是很困难的，比如支付宝和淘宝用的就是不同的HSF分支，因为加功能时改了核心代码，不得不拷一个分支单独发展，HSF现阶段就算开源出来，也很难复用，除非对架构重写。
3. **HSF依赖比较多内部系统**，比如配置中心，通知中心，监控中心，单点登录等等，如果要开源还需要做很多剥离工作，而Dubbo为每个系统的集成都留出了扩展点，并已梳理干清所有依赖，同时为开源社区提供了替代方案，用户可以直接使用。
4. **Dubbo比HSF的功能更多**，除了ClassLoader隔离，Dubbo基本上是HSF的超集，Dubbo也支持更多协议，更多注册中心的集成，以适应更多的网站架构。[![Top](https://www.iteye.com/images/wiki/top.gif?1448702469)](https://www.iteye.com/magazines/103#top)

## 3. HSF配置

### 3.1  引入需要的依赖

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.alibaba.edas</groupId>
    <artifactId>hsf-pandora-boot-provider</artifactId>
    <version>1.0</version>

    <properties>
        <java.version>1.8</java.version>
        <spring-boot.version>2.1.6.RELEASE</spring-boot.version>
        <pandora-boot.version>2019-06-stable</pandora-boot.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>com.alibaba.boot</groupId>
            <artifactId>pandora-hsf-spring-boot-starter</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
        </dependency>
        <dependency>
            <groupId>com.taobao.pandora</groupId>
            <artifactId>taobao-hsf.sar</artifactId>
        </dependency>

        <!-- for unit test-->
        <dependency>
            <groupId>com.taobao.pandora</groupId>
            <artifactId>pandora-boot-test</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
            <dependency>
                <groupId>com.taobao.pandora</groupId>
                <artifactId>pandora-boot-starter-bom</artifactId>
                <version>${pandora-boot.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.7.0</version>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.taobao.pandora</groupId>
                <artifactId>pandora-boot-maven-plugin</artifactId>
                <version>2.1.11.8</version>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>


</project>
```



### 3.2 配置provider

#### 3.2.1 通过Api编程的方式配置

```java
   /**
     * 通过Api编程的方式配置
     * @param helloService
     * @return
     * @throws Exception
     */
    @Bean(name = "helloServiceProvider")
    public HSFApiProviderBean helloServiceImpl(HelloService helloService) throws Exception {
        /* 实例化并配置 Provider Bean*/
        HSFApiProviderBean hsfApiProviderBean = new HSFApiProviderBean();
        hsfApiProviderBean.setServiceInterface("com.alibaba.edas.HelloService");
        hsfApiProviderBean.setTarget(helloService); // target 为 serviceInterface 指定接口的实现对象
        hsfApiProviderBean.setServiceVersion("1.0.0");
        hsfApiProviderBean.setServiceGroup("HSF");

        // 初始化 Provider Bean，发布服务
        hsfApiProviderBean.init();
        return hsfApiProviderBean;
    }
```

#### 3.2.2 Spring 配置方式 

通过在 Spring 配置文件中，配置一个 class 为 com.taobao.hsf.app.spring.util.HSFSpringProviderBean 的 bean，即可完成 HSF 服务的发布。

示例代码

```
<bean id="helloService" class="com.alibaba.edas.HelloServiceImpl" />

<bean class="com.taobao.hsf.app.spring.util.HSFSpringProviderBean" init-method="init">
    <!-- [必选] 设置 HSF 服务对外提供的业务接口 -->
    <property name="serviceInterface" value="com.alibaba.edas.HelloService" />

    <!-- [必选] 设置 serviceInterface 指定接口的服务实现对象，即需要发布为 HSF 服务的 spring bean id -->
    <property name="target" ref="helloService" />

    <!-- [可选] 设置服务的版本号，默认为 1.0.0 -->
    <property name="serviceVersion" value="1.0.0" />

    <!-- [可选] 设置服务的组别，默认为 HSF -->
    <property name="serviceGroup" value="HSF" />

    <!-- [可选] 设置服务的描述，从而方便管理，默认为 null -->
    <property name="serviceDesc" value="HelloWorldService providered by HSF" />

    <!-- [可选] 设置响应超时时间（单位：毫秒）。如果服务端在设置的时间内没有返回，则抛出 HSFTimeOutException -->
    <!-- 默认为 3000 ms -->
    <property name="clientTimeout" value="3000"/>

    <!-- [可选] 设置服务中某些方法的响应超时时间。优先级高于上面的 clientTimeout -->
    <!-- 通过设置 MethodSpecial.methodName 指定方法名，MethodSpecial.clientTimout 指定方法的超时时间 -->
    <property name="methodSpecials">
        <list>
            <bean class="com.taobao.hsf.model.metadata.MethodSpecial">
                <property name="methodName" value="sum" />
                <property name="clientTimeout" value="2000" />
            </bean>
        </list>
    </property>

    <!-- [可选] 设置服务的请求参数和响应结果的序列化方式。可选值有：java，hessian，hessian2，json，kryo -->
    <!-- 默认为 hessian2 -->
    <property name="preferSerializeType" value="hessian2"/>

    <!-- [可选] 配置服务单独的线程池，并指定最小活跃线程数量。若不设置该属性，则默认使用 HSF 服务端的公共线程池 -->
    <property name="corePoolSize" value="10"/>

    <!-- [可选] 配置服务单独的线程池，并指定最大活跃线程数量。若不设置该属性，则默认使用 HSF 服务端的公共线程池 -->
    <property name="maxPoolSize" value="60"/>
</bean>
```

#### 3.2.3 通过注解

```java
@HSFProvider(serviceInterface = HelloService.class, serviceVersion = "1.0.0")
public class HelloServiceImpl implements HelloService {
    ...
}
```



### 3.3 配置customer

#### 3.3.1 通过Api编程的方式配置

```java
/**
  * 通过Api编程的方式配置
  * @param helloService
  * @return
  * @throws Exception
  */
@Bean(name = "helloService")
public HelloService helloService() throws Exception {
    // 实例化并配置 Consumer Bean
    HSFApiConsumerBean hsfApiConsumerBean = new HSFApiConsumerBean();
    hsfApiConsumerBean.setInterfaceName("com.alibaba.edas.HelloService");
    hsfApiConsumerBean.setVersion("1.0.0");
    hsfApiConsumerBean.setGroup("HSF");

    // 初始化 Consumer Bean，订阅服务
    // true 表示等待地址推送（超时时间为 3000 毫秒），默认为 false（异步）
    hsfApiConsumerBean.init(true);

    // 获取 HSF 代理
    HelloService helloService = (HelloService) hsfApiConsumerBean.getObject();
    return helloService;
}
```

#### 3.3.2 Spring 配置方式

通过在 Spring 配置文件中，配置一个 class 为 com.taobao.hsf.app.api.util.HSFSpringConsumerBean 的 bean，即可实现服务的订阅。

示例代码

```
<bean id="helloService" class="com.taobao.hsf.app.spring.util.HSFSpringConsumerBean">
    <!-- [必选] 设置需要订阅服务的接口名 -->
    <property name="interfaceName" value="com.alibaba.edas.HelloService" />

    <!-- [必选] 设置需要订阅服务的版本号 -->
    <property name="version" value="1.0.0" />

    <!-- [必选] 设置需要订阅服务的组别 -->
    <property name="group" value="HSF" />

    <!-- [可选] 设置请求超时时间（单位：毫秒）。如果客户端在设置的时间内没有收到服务端响应，则抛出 HSFTimeOutException -->
    <!-- 若客户端设置了 clientTimeout，则优先级高于服务端设置的 clientTimeout。否则，在服务的远程调用过程中，使用服务端设置的 clientTimeout。-->
    <property name="clientTimeout" value="3000" />

    <!-- [可选] 设置服务中某些方法的请求超时时间，优先级高于当前客户端的 clientTimeout -->
    <!-- 通过设置 MethodSpecial.methodName 指定方法名，通过设置 MethodSpecial.clientTimout 指定当前方法的超时时间 -->
    <property name="methodSpecials">
        <list>
            <bean class="com.taobao.hsf.model.metadata.MethodSpecial">
                <property name="methodName" value="sum" />
                <property name="clientTimeout" value="2000" />
            </bean>
        </list>
    </property>

    <!-- [可选] 设置同步等待 ConfigServer 推送地址的时间（单位：毫秒）-->
    <!-- 从而避免因地址还未推送到就发起服务调用造成的 HSFAddressNotFoundException。-->
    <!-- 一般建议设置为 5000 毫秒，即可满足推送等待时间。 -->
    <property name="maxWaitTimeForCsAddress"  value="5000"/>

    <!-- [可选] 设置需要异步调用的方法列表。List 中的每一个字符串的格式为：name: 方法名;type: 异步调用类型;listener: 监听器 -->
    <!-- 其中，listener 只对 callback 类型的异步调用生效。type 的类型有：-->
    <!-- future: 通过 Future 的方式去获取请求执行的结果 -->
    <!-- callback: 当远程服务的调用完成后，HSF 会使用响应结果回调此处配置的 listener，该 listener 需要实现 HSFResponseCallback 接口 -->
    <property name="asyncallMethods">
        <list>
            <value>name:sayHello;type:callback;listener:com.taobao.hsf.test.service.HelloWorldServiceCallbackHandler</value>
        </list>
    </property>

    <!-- [可选] 设置服务的代理模式，一般不用配置。如果要拦截这个 consumer bean，需要配置成 javassist -->
    <property name="proxyStyle" value="jdk" />
</bean>
```

#### 3.3.3 通过注解

```java
@HSFConsumer(clientTimeout = 3000, serviceVersion = "1.0.0")
```

## 4. HSF部署

#### 4.1 使用 Pandora Boot 开发应用

##### 4.1.1 配置 EDAS 的私服地址和轻量配置中心

- [在 Maven 中配置 EDAS 的私服地址](https://help.aliyun.com/knowledge_detail/66643.html?spm=a2c4g.11186623.6.594.45a36302BNXt9S)
- [启动轻量级配置及注册中心](https://help.aliyun.com/document_detail/142182.html#task-2310117)

#### 4.2 在IDE中启动

通过 VM options 配置启动参数 -Djmenv.tbsite.net={$IP}，通过 main 方法直接启动。其中 `{$IP}` 为轻量配置中心的 IP 地址。列如本机启动轻量配置中心，则 `{$IP}` 为 `127.0.0.1`。

您也可以不配置 JVM 的参数，而是直接通过修改 hosts 文件将 `jmenv.tbsite.net` 绑定为轻量配置中心的 IP。详情请参见[启动轻量级配置及注册中心](https://help.aliyun.com/document_detail/44163.html#task-2310117)。

#### 4.3 通过 FatJar 启动

1. 在工程的主目录下，执行 maven 命令 `mvn clean package` 进行打包，即可在 Target 目录下找到打包好的 FatJar 文件。

2. 通过 Java 命令启动应用

   ```cmd
   java -Djmenv.tbsite.net=127.0.0.1 -Dpandora.location=${M2_HOME}/.m2/repository/com/taobao/pandora/taobao-hsf.sar/2019-06-stable/taobao-hsf.sar-2019-06-stable.jar -jar hsf-pandora-boot-provider-1.0.jar
   ```

## 5. 参考文档

1. [阿里云HSF-Api手册](https://help.aliyun.com/document_detail/140490.html?spm=a2c4g.11186623.6.607.6fa21aafnDOkEl#title-a4b-o2n-cny)
2. [EDAS开发指南pdf](https://gitee.com/toutoukeji_admin/codes/0iocmsjy5wrf6pkne2vqh90#0-tsina-1-78717-397232819ff9a47a7b7e80a40613cfe1)






